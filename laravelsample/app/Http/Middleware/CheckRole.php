<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role !== 'admin') {
            $error = \Illuminate\Validation\ValidationException::withMessages([
             'admin role' => ['Requested URL is only allowed to admin users.'],
            ], url()->previous());
            throw $error;
        }
        return $next($request);
    }
}
