<?php

namespace App\Http\Requests\TimeRecord;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Index extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'user_id' => 'integer|min:1',
          'year' => 'integer|min:1970',
          'month' => 'integer|min:1|max:12',
        ];
    }

    /**
     * Get the route params to validate.
     * @return array
     */
    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'user_id' => (int)$this->user_id,
            'year' => (int)$this->year,
            'month' => (int)$this->month,
        ]);
    }
}
