<?php

namespace App\Http\Requests\TimeRecord;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'user_id' => 'required|integer|min:1',
          'begin' => 'nullable|date_format:H:i',
          'finish' => 'nullable|date_format:H:i',
          'rest' => 'nullable|numeric|min:0',
          'memo' => 'nullable|max:512',
          'year' => 'required|integer|min:1970',
          'month' => 'required|integer|min:1|max:12',
          'day' => 'required|integer|min:1|max:31',
        ];
    }
}
