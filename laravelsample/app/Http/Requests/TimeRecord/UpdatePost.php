<?php

namespace App\Http\Requests\TimeRecord;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'begin' => 'nullable|date_format:H:i',
          'finish' => 'nullable|date_format:H:i',
          'rest' => 'nullable|numeric|min:0|max:24',
          'memo' => 'max:512',
        ];
    }
}
