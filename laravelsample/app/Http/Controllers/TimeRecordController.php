<?php

namespace App\Http\Controllers;

use App\TimeRecord;
use App\Http\Requests\TimeRecord\Index;
use App\Http\Requests\TimeRecord\CreateRequest;
use App\Http\Requests\TimeRecord\StorePost;
use App\Http\Requests\TimeRecord\UpdatePost;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;

class TimeRecordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of resource users.
     *
     * @return \Illuminate\Http\Response
     */
    public function userlist()
    {
      $now = CarbonImmutable::now();
      if (Auth::user()->role === 'admin') {
        $users = \App\User::paginate(5);
        return view('time_record.user_list', ['users' => $users, 'now' => $now]);
      } else {
        return redirect(
          implode('/', ['time_records', Auth::user()->id, $now->year, $now->month])
        );
      }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\TimeRecord\Index  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Index $indexRequest)
    {
      $params = $indexRequest->validated();
      $user_id = $params['user_id'];
      $year = $params['year'];
      $month = $params['month'];

      $this->validateUserId($user_id);

      $time_records = \App\TimeRecord::where('user_id', $user_id)
        ->whereYear('work_date', $year)
        ->whereMonth('work_date', $month)
        ->orderBy('work_date', 'asc')
        ->get();

      $begin = CarbonImmutable::createSafe($year, $month, 1, 0, 0, 0, 'UTC');
      $lastmonth = $begin->sub(1, 'month');
      if ($lastmonth->year < 1970) {
        $lastmonth = null;
      }
      $nextmonth = $begin->add(1, 'month');
      $end = $nextmonth->sub(1, 'day');
      $interval = CarbonInterval::days(1);
      $period = CarbonPeriod::create($begin, $interval, $end);

      $calendar = [];
      foreach ($period as $day) {
        $calendar_row = [
          "day" => $day->day,
          'begin' => '',
          'finish' => '',
          'rest' => '',
          'work_time' => '',
          'memo' => '',
          'created_at' => '',
          'updated_at' => '',
          'timerecord_id' => false,
        ];
        foreach ($time_records as $t) {
          $work_date = CarbonImmutable::createFromFormat('Y-m-d', $t->work_date, 'UTC');
          if ($work_date->isSameDay($day)) {
            $calendar_row = array_merge($calendar_row, [
              'begin' => $t->begin,
              'finish' => $t->finish,
              'rest' => $t->rest,
              'work_time' => $t->work_time,
              'memo' => $t->memo,
              'created_at' => $t->created_at,
              'updated_at' => $t->updated_at,
              'timerecord_id' => $t->id,
            ]);
            break;
          }
        }
        array_push($calendar, (object) $calendar_row);
      }

      return view('time_record.index', [
        'user_id' => $user_id,
        'year' => $year,
        'month' => $month,
        'calendar' => $calendar,
        'lastmonth' => $lastmonth,
        'nextmonth' => $nextmonth,
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Http\Requests\TimeRecord\CreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreateRequest $request)
    {
        $params = $request->validated();
        $this->validateDate($params['year'], $params['month'], $params['day']);
        return view('time_record.create', [
          'user_id' => $params['user_id'],
          'year' => $params['year'],
          'month' => $params['month'],
          'day' => $params['day'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TimeRecord\StorePost  $storePost
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $storePost)
    {
      $params = $storePost->validated();

      $this->validateDate($params['year'], $params['month'], $params['day']);
      $work_date = CarbonImmutable::create($params['year'], $params['month'], $params['day']);
      $user_id = $params['user_id'];
      $time_records = \App\TimeRecord::where('user_id', $user_id)
        ->whereYear('work_date', $work_date->year)
        ->whereMonth('work_date', $work_date->month)
        ->whereDay('work_date', $work_date->day)
        ->get();

      if (count($time_records) > 0) {
        $error = \Illuminate\Validation\ValidationException::withMessages([
         'created_at' => ['Invalid created_at: the record already created.'],
        ], url()->previous());
        throw $error;
      } else {
        $now = CarbonImmutable::now();
        $work_time = $this->calcWorkTime($params['begin'], $params['finish'], $params['rest']);
        $time_record = new TimeRecord;
        $time_record->user_id = $user_id;
        $time_record->begin = $params['begin'];
        $time_record->finish = $params['finish'];
        $time_record->work_date = $work_date->toDate();
        $time_record->rest = $params['rest'];
        $time_record->work_time = $work_time;
        $time_record->memo = $params['memo'];
        $time_record->created_at = $now;
        $time_record->updated_at = $now;
        $time_record->save();
        return redirect(
          implode('/', ['time_records', $user_id, $now->year, $now->month])
        );
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TimeRecord  $timeRecord
     * @return \Illuminate\Http\Response
     */
    /**
    public function show(TimeRecord $timeRecord)
    {
        //
    }
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TimeRecord  $timeRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeRecord $timeRecord)
    {
        $this->validateRecordUserId($timeRecord);
        return view('time_record.edit', ['time_record' => $timeRecord]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreTimeRecordPost  $storePost
     * @param  \App\TimeRecord  $timeRecord
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePost $updatePost, TimeRecord $timeRecord)
    {
        $this->validateRecordUserId($timeRecord);
        $params = $updatePost->validated();

        $work_time = $this->calcWorkTime($params['begin'], $params['finish'], $params['rest']);

        $timeRecord->update([
          'begin' => $params['begin'],
          'finish' => $params['finish'],
          'rest' => $params['rest'],
          'work_time' => $work_time,
          'memo' => $params['memo'],
        ]);
        $t = CarbonImmutable::createFromTimeString($timeRecord->created_at);
        return redirect(
          implode('/', ['time_records', $timeRecord->user_id, $t->year, $t->month])
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TimeRecord  $timeRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeRecord $timeRecord)
    {
        $this->validateRecordUserId($timeRecord);
        $t = CarbonImmutable::createFromTimeString($timeRecord->created_at);
        $timeRecord->delete();
        return redirect(
          implode('/', ['time_records', $timeRecord->user_id, $t->year, $t->month])
        );
    }

    /**
     * Throw error and user back if the timerecord user_id is invalid.
     *
     * @param  \App\TimeRecord  $timeRecord
     */
    private function validateRecordUserId(TimeRecord $timeRecord)
    {
      $this->validateUserId($timeRecord->user_id);
    }

    /**
     * Throw error and user back if the user_id is invalid.
     *
     * @param  \App\TimeRecord  $timeRecord
     */
    private function validateUserId(int $user_id)
    {
        if (Auth::user()->id !== $user_id && Auth::user()->role !== 'admin') {
            $error = \Illuminate\Validation\ValidationException::withMessages([
             'user_id' => ['Invalid user_id'],
            ], url()->previous());
            throw $error;
        }
    }

    /**
     * Throw error and user back if given date is invalid.
     *
     * @param  int  $year
     * @param  int  $month
     * @param  int  $day
     */
    private function validateDate(int $year, int $month, int $day)
    {
        if (!checkdate($month, $day, $year)) {
            $error = \Illuminate\Validation\ValidationException::withMessages([
             'user_id' => ['Invalid date: '.$year.'-'.$month.'-'.$day],
            ], url()->previous());
            throw $error;
        }
    }

    /**
     * Throw error and user back if given begin and finish are invalid.
     *
     * @param  CarbonImmutable  $begin
     * @param  CarbonImmutable  $finish
     */
    private function validateWorkTime(CarbonImmutable $begin, CarbonImmutable $finish)
    {
        if ($begin->isAfter($finish)) {
            $error = \Illuminate\Validation\ValidationException::withMessages([
             'user_id' => ['Invalid work time begin and finish'],
            ], url()->previous());
            throw $error;
        }
    }

    /**
     * Calculate work time.
     *
     * @param  $beginTime
     * @param  $finishTime
     * @param  $restTime
     * @return $work_time
     */
    private function calcWorkTime($beginTime, $finishTime, $restTime)
    {
        if (is_null($beginTime) || is_null($finishTime)) {
          return 0;
        }
        $begin = CarbonImmutable::createFromFormat('H:i', $beginTime, 'UTC');
        $finish = CarbonImmutable::createFromFormat('H:i', $finishTime, 'UTC');
        $this->validateWorkTime($begin, $finish);

        $rest = 0;
        if (!is_null($restTime)) {
          $rest = (float) $restTime;
        }
        $work_time = $begin->diffInMinutes($finish) - 60 * $rest;
        if ($work_time < 0) {
          $work_time = 0;
        }
        return $work_time / 60;
    }
}
