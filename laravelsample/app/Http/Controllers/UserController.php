<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\User\UpdatePost;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \App\User::paginate(5);
        return view('user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
    public function create()
    {
        //
    }
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
    public function store(Request $request)
    {
        //
    }
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
    public function show($id)
    {
        //
    }
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePost $updatePost, User $user)
    {
        $request = $updatePost->validated();
        if ($user->id === 1 && $request['role'] !== 'admin') {
            $error = \Illuminate\Validation\ValidationException::withMessages([
             'admin user' => ['Admin user(id:1) is not allowed to be general user.'],
            ], url()->previous());
            throw $error;
        }
        $user->update($request);
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->id === 1) {
            $error = \Illuminate\Validation\ValidationException::withMessages([
             'admin user' => ['Admin user(id:1) is not allowed to delete.'],
            ], url()->previous());
            throw $error;
        }
        $user->delete();
        return redirect('users');
    }
}
