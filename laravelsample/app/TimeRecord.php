<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeRecord extends Model
{
  protected $fillable = [
    'begin', 'finish', 'rest', 'work_time', 'memo'
  ];

    /**
     * User who stamped this record.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
