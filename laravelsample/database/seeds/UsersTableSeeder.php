<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@hoge.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin'),
            'role' => 'admin',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('users')->insert([
            'name' => 'hoge',
            'email' => 'hoge@hoge.com',
            'email_verified_at' => now(),
            'password' => bcrypt('hogehoge'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
