<?php

use App\TimeRecord as TimeRecord;
use Illuminate\Database\Seeder;

use Carbon\CarbonImmutable;

class TimeRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TimeRecord::class, 120)->create()->each(function ($time_record) {
            $now = CarbonImmutable::today();
            $created_at = $now->sub(1, 'month')
              ->add($time_record->id, 'day')
              ->setTimeFromTimeString($time_record->begin);
            $finish = $created_at
              ->setTimeFromTimeString($time_record->begin)->add(
              $time_record->rest * 60 + $time_record->work_time * 60,
              'minute'
            );
            $time_record->work_date = $created_at->toDate();
            $time_record->created_at = $created_at;
            $time_record->finish = $finish;
            $time_record->updated_at = $finish;
						$time_record->save();
				});
    }
}
