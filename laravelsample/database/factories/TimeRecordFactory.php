<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\TimeRecord as TimeRecord;
use Faker\Generator as Faker;

use Carbon\CarbonImmutable;

$factory->define(TimeRecord::class, function (Faker $faker) {
    return [
      'user_id' => $faker->randomElement([1, 2]),
      'begin' => '0'.$faker->numberBetween(0, 8).':'.
        $faker->numberBetween(0, 59).':'.
        $faker->numberBetween(0, 59)
      ,
      'rest' => $faker->randomFloat(1, 0, 1.5),
      'work_time' => $faker->randomFloat(1, 1, 12),
      'finish' => null,
      'memo' => $faker->text(32),
      'created_at' => null,
      'updated_at' => null,
    ];
});
