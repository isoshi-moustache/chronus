@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">TimeRecords (user_id:{{ $user_id }}) {{ $year }} / {{ $month }}</div>

                <div class="card-body">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif

                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">day</th>
                                <th scope="col">begin</th>
                                <th scope="col">finish</th>
                                <th scope="col">rest</th>
                                <th scope="col">work_time</th>
                                <th scope="col">memo</th>
                                <th scope="col">created_at</th>
                                <th scope="col">updated_at</th>
                                <th scope="col">create / edit</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach ($calendar as $tr)
                            <tr>
                                <td>{{ $tr->day }}</td>
                                <td>{{ implode(':', explode(':', $tr->begin, -1)) }}</td>
                                <td>{{ implode(':', explode(':', $tr->finish, -1)) }}</td>
                                <td>{{ $tr->rest }}</td>
                                <td>{{ $tr->work_time }}</td>
                                <td>{{ $tr->memo }}</td>
                                <td>{{ $tr->created_at }}</td>
                                <td>{{ $tr->updated_at }}</td>
                                <td>
                                  @if ($tr->timerecord_id)
                                    <a href="/time_records/{{ $tr->timerecord_id }}/edit">edit</a>
                                  @else
                                    <a href="/time_records/create/{{ $user_id }}/{{ $year }}/{{ $month }}/{{ $tr->day }}/">create</a>
                                  @endif
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>

                    <div class="mx-auto" style="width: 200px">
                      @if ($lastmonth)
                        <a href="/time_records/{{ $user_id }}/{{ $lastmonth->year }}/{{ $lastmonth->month }}/">last month</a>
                      @endif
                        <a href="/time_records/{{ $user_id }}/{{ $nextmonth->year }}/{{ $nextmonth->month }}/">next month</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
