@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">TimeRecord Edit (id:{{ $time_record->id }} date:{{ $time_record->work_date }})</div>

                <div class="card-body">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif
                    <form method="POST" action="/time_records/{{ $time_record->id }}">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="begin">begin</label>
                            <input type='time' class='form-control' name='begin' value="{{ implode(':', explode(':', $time_record->begin, -1)) }}">
                            <label for="finish">finish</label>
                            <input type='time' class='form-control' name='finish' value="{{ implode(':', explode(':', $time_record->finish, -1)) }}">
                            <label for="rest">rest</label>
                            <input type='text' class='form-control' name='rest' placeholder="{{ $time_record->rest }}" value="{{ $time_record->rest }}">
                            <label for="memo">memo</label>
                            <input type='text' class='form-control' name='memo' placeholder="{{ $time_record->memo }}" value="{{ $time_record->memo }}">
                        </div>
                        <input type="submit" value="submit">
                    </form>
                    <form method="POST" action="/time_records/{{ $time_record->id }}">
                        @method('DELETE')
                        @csrf
                        <input type="submit" value="delete record">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
