@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">TimeRecord Create(user_id:{{ $user_id }} date:{{ $year }}-{{ $month }}-{{ $day }})</div>

                <div class="card-body">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif
                    <form method="POST" action="/time_records">
                        @csrf
                        <div class="form-group">
                            <label for="begin">begin</label>
                            <input type='time' class='form-control' name='begin'>
                            <label for="finish">finish</label>
                            <input type='time' class='form-control' name='finish'>
                            <label for="rest">rest</label>
                            <input type='text' class='form-control' name='rest' placeholder="0.0">
                            <label for="memo">memo</label>
                            <input type='text' class='form-control' name='memo' placeholder="Max 512 bytes.">
                            <input type='hidden' name='user_id' value="{{ $user_id }}">
                            <input type='hidden' name='year' value="{{ $year }}">
                            <input type='hidden' name='month' value="{{ $month }}">
                            <input type='hidden' name='day' value="{{ $day }}">
                        </div>
                        <input type="submit" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
