@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">User Edit (id:{{ $user->id }})</div>

                <div class="card-body">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif
                    <form method="POST" action="/users/{{ $user->id }}">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">name</label>
                            <input type='text' class='form-control' name='name' placeholder="John Doe" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email addr</label>
                            <input type='email' class='form-control' name='email' placeholder="name@example.com" value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label for="role">admin role</label>
                            <select class='form-control' name='role'>
                                <option @if ($user->role === 'admin') selected @endif>admin</option>
                                <option @if ($user->role === 'user') selected @endif>user</option>
                            </select>
                        </div>
                        <input type="submit" value="submit">
                    </form>
                    <form method="POST" action="/users/{{ $user->id }}">
                        @method('DELETE')
                        @csrf
                        <input type="submit" value="delete user">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
