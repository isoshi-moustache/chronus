<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('users', 'UserController')->except([
  'create', 'store', 'show'
]);

Route::get('time_records', 'TimeRecordController@userlist');
Route::get('time_records/{user_id}/{year}/{month}', 'TimeRecordController@index');
Route::get('time_records/create/{user_id}/{year}/{month}/{day}', 'TimeRecordController@create');
Route::resource('time_records', 'TimeRecordController')->except([
  'create', 'index', 'show'
]);

Route::get('time_record_users', 'TimeRecordUsersController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
