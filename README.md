# laravelsample

## initialize

```
git clone https://github.com/Laradock/laradock.git
```

### .env file

```
cd laradock
cp env-example .env
```

edit `.env` environment variable to app dir.

```
# Point to the path of your applications code on your host
APP_CODE_PATH_HOST=../laravelsample/
```

```
MYSQL_VERSION=5.7
```


```
docker-compose up -d nginx mysql redis workspace maildev
```

### workspace

```
docker-compose exec --user=laradock workspace bash
```

### install php packages and generate app key

```
cp env-example .env
composer install
php artisan key:generate
php artisan migrate
composer dump-autoload
php artisan db:seed
```

## URLs

| path | description |
----|---- 
| /register | user registration |
| /login | login page |
| /home | default page |
| /users | user list |
| /password/reset | send email to reset password |
